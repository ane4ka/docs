export REPLICAS=1
export URL="https://v2.postgres.ai"
export BASE_URL="/"
export SIGN_IN_URL="https://console-v2.postgres.ai/signin"
