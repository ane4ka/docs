export const nik = {
  avatarUrl: '/assets/author/nik.jpg',
  name: 'Nikolay Samokhvalov',
  role: 'CEO & Founder of',
  twitterUrl: 'https://twitter.com/samokhvalov',
  gitlabUrl: 'https://gitlab.com/NikolayS',
  githubUrl: 'https://github.com/NikolayS',
  linkedinUrl: 'https://www.linkedin.com/in/samokhvalov',
  note: 'Working on tools to balance Dev with Ops in DevOps',
}

export const dante = {
  avatarUrl: '/assets/author/dante.jpg',
  name: 'Dante Cassanego',
  twitterUrl: 'https://twitter.com/dantec',
  gitlabUrl: 'https://gitlab.com/dante.cassanego',
  githubUrl: 'https://github.com/dcassanego',
  linkedinUrl: 'https://www.linkedin.com/in/dantecassanego',
}

export const anatoly = {
  avatarUrl: '/assets/author/anatoly.jpg',
  name: 'Anatoly Stansler',
  gitlabUrl: 'https://gitlab.com/anatolystansler',
  githubUrl: 'https://github.com/anatolystansler',
  linkedinUrl: 'https://www.linkedin.com/in/anatoly-stansler-37265514a',
}

export const artyom = {
  avatarUrl: '/assets/author/artyom.jpg',
  name: 'Artyom Kartasov',
  role: 'Software Engineer at',
  twitterUrl: 'https://twitter.com/arkartasov',
  gitlabUrl: 'https://github.com/agneum',
  githubUrl: 'https://gitlab.com/akartasov',
}
