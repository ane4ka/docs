const careers = [
  {
    title: 'Senior Database Engineer | SQL Optimization Expert',
    descriptions: [
      'Strong optimization skills of large and heavily loaded databases (>1TiB, >10k QpS)',
      '3+ years of experience running PostgreSQL in large production environments',
      'Advanced SQL and PL/pgSQL knowledge',
    ],
    link: '/careers/dba',
  },
  {
    title: 'Senior Full Stack Developer | Go | React',
    descriptions: [
      '3+ years experience developing server applications using Go',
      'Strong experience in client-side development using React',
      'Solid skills of basic SQL (SQL-92)',
    ],
    link: '/careers/fullstack',
  },
  {
    title: 'Senior Software Engineer | Go',
    descriptions: [
      '3+ years experience developing server applications using Go',
      'Deep understanding of HTTP protocol, data structures, JSON',
      'REST API development experience',
      'Solid skills of basic SQL (SQL-92)',
    ],
    link: '/careers/godeveloper',
  },
];

export default careers;
